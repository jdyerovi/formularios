<?php

namespace App\Http\Controllers;

use App\Form;
use App\Question;
use App\Answer;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forms = Form::all();
        return view('form.index')->with('forms', $forms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if (!$validator->fails()) {
            $form = new Form;
            $form->name = $request->get('name');
            $form->save();

            return redirect('form');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formulario = Form::find($id);
        $questions = $formulario->questions;

        return view('form.edit')->with('formulario', $formulario)->with('questions', $questions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'id'   => 'required',
            'name' => 'required|max:255',
        ]);

        if (!$validator->fails()) {
            $form = Form::find($request->get('id'));
            $form->name = $request->get('name');
            $form->save();

            return redirect('form/edit/'.$request->get('id'));
        }
    }

    public function questionEdit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if (!$validator->fails()) {

            $id = $request->get('id');
            if (isset($id)) {
                $question = Question::find($request->get('id'));
            } else {
                $question = new Question();
                $question->form_id = $request->get('form_id');
            }

            $question->question = $request->get('name');
            $question->save();

            return redirect('form/edit/'.$question->form_id);
        }
    }

    public function questionDelete(Request $request)
    {
        $question = Question::find($request->get('id'));
        $form_id = $question->form_id;
        $question->delete();

        return redirect('form/edit/'.$form_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function answers($id) {
        $question = Question::find($id);

        $respuestas = $question->answers;

        return view('form.asnwers')->with('answers', $respuestas)->with('question', $question);
    }

    public function answersAdd(Request $request) {
        
        $id = $request->get('id');

        if (isset($id)) {
            $answer = Answer::find($request->get('id'));
        } else {
            $answer = new Answer();
            $answer->question_id = $request->get('question_id');
        }

        $answer->description = $request->get('name');
        $answer->value = $request->get('value');
        $answer->save();

        return redirect('question/'.$answer->question_id);

    }

    public function send(Request $request) {

        $questions = $request->get('questions');
        $preguntas = $request->get('pregunta');
        $fullanswer = $request->get('answerFull2');
        $count = count($questions);

        $big= [];

        foreach ($preguntas as $key => $pregunta){
            $answer = Answer::find(explode('_',$key)[1]);
            $big[] = [
                'question' => $questions[$key],
                'preguntas' => $preguntas[$key],
                'full' => $fullanswer[$key]
            ];
        }

      // var_dump($big);die;

        \Mail::send('sendform2', ['bigdata' => $big, 'email' => $request->get('email')], function ($m) {

            $m->to('cedife1234@gmail.com','Cedife')->subject('Formulario recibido');
        });
        return view('sendform2')->with('bigdata', $big)->with('email', $request->get('email'));
        //var_dump($request);
    }
}
