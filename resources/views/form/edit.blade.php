@extends('welcome')
@section('form')

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Nombre del formulario</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form method="POST">
                <div class="row">
                    <div class="col-md-10">
                        <input type="hidden" name="id" value="{{ $formulario->id }}">
                        <input type="text" class="form-control" name="name" value="{{ $formulario->name }}">
                    </div>
                    <div class="col-md-2">
                        <input type="submit" class="btn btn-success" value="Guardar">
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Preguntas</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @foreach($questions as $question)
                <div class="row">
                    <div class="col-md-9">
                        <form method="POST" action="{{ url('question/edit') }}">
                            <div class="row">
                                <div class="col-md-11">
                                    <input type="hidden" name="id" value="{{ $question->id }}">
                                    <input type="text" class="form-control" name="name" value="{{ $question->question }}">
                                </div>
                                <div class="col-md-1">
                                    <input type="submit" class="btn btn-success"  value="Guardar">
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form method="POST" action="{{ url('question/delete') }}">
                            <input type="hidden" name="id" value="{{ $question->id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn btn-danger" value="Eliminar">
                            <a href='{{ url('question') }}/{{ $question->id }}'>
                                <input type="button" class="btn btn-blue" value="Respuestas">
                            </a>
                        </form>
                    </div>
                </div>
                <br><br>
            @endforeach

            <h3 class="box-title">Nueva Pregunta</h3>

                <div class="row">
                    <div class="col-md-10">
                        <form method="POST" action="{{ url('question/edit') }}">
                            <div class="row">
                                <div class="col-md-11">
                                    <input type="hidden" name="form_id" value="{{ $formulario->id }}">
                                    <input type="text" class="form-control" name="name" value="">
                                </div>
                                <div class="col-md-1">
                                    <input type="submit" class="btn btn-success" value="Guardar">
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>




        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
    </div>

@endsection