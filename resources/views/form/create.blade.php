@extends('welcome')
@section('form')

<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">formulario create</h3>
<div class="box-body">
	<form method="POST">
		<div class="row">
         	<div class="col-md-10">
	    		<input type="text" class="form-control" name="name">
			</div>
			<div class="col-md-2">
	    		<input type="submit" class="btn btn-success" value="Guardar">
	    	</div>
	    </div>
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	</form>
</div>
@endsection
