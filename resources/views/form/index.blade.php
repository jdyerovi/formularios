@extends('welcome')
@section('form')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Bordered Table</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody><tr>
                            <th style="width: 10px">Id</th>
                            <th>Form Name</th>
                            <th style="width: 30%">Actions</th>
                        </tr>
                        @foreach($forms as $form)
                        <tr>
                            <td>{{ $form->id }}</td>
                            <td>{{ $form->name }}</td>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <a href="form/edit/{{ $form->id }}">
                                            <button type="button" class="btn btn-block btn-danger">Delete</button>
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="form/edit/{{ $form->id }}">
                                            <button type="button" class="btn btn-block btn-primary">Edit</button>
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a href="form/{{ $form->id }}">
                                            <button type="button" class="btn btn-block btn-success">Ver</button>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
@endsection
