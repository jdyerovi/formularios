<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'auth'], function () {
    Route::get('form', 'FormController@index');
    Route::get('form/create', 'FormController@create');
    Route::post('form/create', 'FormController@store');
    Route::get('form/edit/{id}', 'FormController@edit');
    Route::post('form/edit/{id}', 'FormController@update');
    Route::get('question/{id}', 'FormController@answers');
    Route::post('question/answers/add', 'FormController@answersAdd');

    Route::post('question/edit', 'FormController@questionEdit');
    Route::post('question/delete', 'FormController@questionDelete');
});




Route::get('/form/{id}', function ($id) {


    $form = \App\Form::find($id);

    $questions = $form->questions;

    //var_dump($form);

    return view('form')->with('questions', $questions);
});

Route::post('form/{id}', 'FormController@send');

Route::get("test-email", function() {
    Mail::send("emails.bienvenido", [], function($message) {
        $message->to("jdyerovi@gmail.com", "Pico")
            ->subject("Picoooo");
    });
});

Route::get("create", function(){
    $user = \App\User::create([
        'name' => 'Cedife',
        'email' => '1magda_tu@hotmail.com',
        'password' => bcrypt('Cedife12345'),
    ]);
    var_dump($user);
});

Route::get("login", 'Auth\AuthController@login');
Route::post("login", 'Auth\AuthController@login2');