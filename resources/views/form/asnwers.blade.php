@extends('welcome')
@section('form')
	@foreach($answers as $answer)
	    <div class="row">
	        <div class="col-md-9">
	            <form method="POST" action="{{ url('question/answers/add') }}">
	                <div class="row">
	                    <div class="col-md-11">
	                        <input type="hidden" name="id" value="{{ $answer->id }}">
	                        <input type="text" class="form-control" name="name" value="{{ $answer->description }}">
	                        Value
                                    <input type="text" class="form-control" name="value" value="{{ $answer->value }}">
	                    </div>
	                    <div class="col-md-1">
	                        <input type="submit" class="btn btn-success"  value="Guardar">
	                    </div>
	                </div>
	                <input type="hidden" name="_token" value="{{ csrf_token() }}">
	            </form>
	        </div>
	        <div class="col-md-3">
	            <form method="POST" action="{{ url('question/delete') }}">
	                <input type="hidden" name="id" value="{{ $answer->id }}">
	                <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                <input type="submit" class="btn btn-danger" value="Eliminar">
	            </form>
	        </div>
	    </div>
	    <br><br>
	@endforeach


	 <h3 class="box-title">Nueva Respuesta</h3>

                <div class="row">
                    <div class="col-md-10">
                        <form method="POST" action="{{ url('question/answers/add') }}">
                            <div class="row">
                                <div class="col-md-11">
                                    <input type="hidden" name="question_id" value="{{ $question->id }}">
                                    <input type="text" class="form-control" name="name" value="">
                                    Value
                                    <input type="text" class="form-control" name="value" value="">
                                </div>
                                <div class="col-md-1">
                                    <input type="submit" class="btn btn-success" value="Guardar">
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </form>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
@endsection	