@extends('welcome2')
@section('form')

<form method="POST">
    Ingrese su correo:<br/> <input type="text" name="email" placeholder="email"><br/><br/><br/>
    @foreach($questions as $question)
        <div class="form-group">
            {{ $question->question  }}

            <input type="hidden" class="full" id="question_{{ $question->id }}" name='answerFull2[question_{{ $question->id }}]'>
            @foreach($question->answers as $answer)
                <div>
                    <input type='hidden' class='answer' name='answerFull[question_{{ $question->id }}]' value="{{ $answer->description  }}">
                    <input type='hidden' name='pregunta[question_{{ $question->id }}]' value="{{ $question->question  }}">
                    <input type="hidden" class="questionm" value="question_{{ $question->id }}">
                    <input style='float:left; margin-right: 10px' type="radio" class='radio' name="questions[question_{{ $question->id }}]" id="questions[question_{{ $question->id }}]" value="{{ $answer->value }}">
                    <label>{{ $answer->description }}</label>
                </div>
            @endforeach

        </div>
    @endforeach
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">Enviar</button>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </div>
</form>


<script>

    $( document ).ready(function() {
        $('.radio').click(function(){
            $preg = $(this).siblings('.answer').val();

            $class = $(this).siblings('.questionm').val();

            $('#'+$class).val($preg);

        });
    });


</script>
@endsection