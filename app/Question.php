<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $timestamps = false;

    public function form() {
        return $this->belongsTo('App\Form');
    }

    public function answers() {
        return $this->hasMany('App\Answer');
    }
}
